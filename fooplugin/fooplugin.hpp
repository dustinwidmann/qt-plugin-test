#ifndef FOOPLUGIN_HPP
#define FOOPLUGIN_HPP

#include <QtCore>
#include <QtPlugin>

#include <foo/foointerface.hpp>

class FooPlugin : public QObject, FooInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "foo.FooPlugin" FILE "fooplugin.json")
    Q_INTERFACES(FooInterface)
public:
    FooPlugin();
    ~FooPlugin();
    void doFoo() override;
};

#endif
