#include <QtDebug>

#include "fooplugin.hpp"

FooPlugin::FooPlugin() = default;

FooPlugin::~FooPlugin() = default;

void FooPlugin::doFoo()
{
    qDebug() << "doing foo!";
}
