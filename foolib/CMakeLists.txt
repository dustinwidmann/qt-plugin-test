cmake_minimum_required(VERSION 3.16.0)

set(FOO_VERSION 0.1.0)
set(FOO_SOVERSION 0.1.0)

include(GNUInstallDirs)

project(foocore
    LANGUAGES CXX
    VERSION ${FOO_VERSION})

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_INSTALL_PREFIX "/tmp/foo")

list(PREPEND CMAKE_PREFIX_PATH "/opt/qt-rms")
find_package(Qt6 COMPONENTS Core REQUIRED)

###############################################################################
###############################################################################
##### Install Foo Library #####
###############################################################################
###############################################################################
set(
    FOOCORE_SOURCES
        src/placeholder.cpp)
set(
    FOOCORE_HEADERS
        include/foo_global.hpp
        include/foointerface.hpp)
add_library(foocore SHARED
    ${FOOCORE_SOURCES}
    ${FOOCORE_HEADERS})
set_target_properties(foocore
    PROPERTIES
        VERSION ${FOO_VERSION}
        SOVERSION ${FOO_SOVERSION}
        MAP_IMPORTED_CONFIG_COVERAGE "DEBUG")
target_link_libraries(foocore
    PRIVATE
        Qt::Core)
target_include_directories(foocore
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src)
target_compile_definitions(foocore
    PRIVATE FOO_LIBRARY)
install(
    TARGETS foocore
    EXPORT foocoreConfig
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install(
    DIRECTORY include/
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/foo)
export(
    TARGETS ${PROJECT_NAME}
    FILE foocoreConfig.cmake
    NAMESPACE foocore::
    FILE "${CMAKE_CURRENT_BINARY_DIR}/foocoreConfig.cmake")
install(
    EXPORT foocoreConfig
    DESTINATION "${CMAKE_INSTALL_DATADIR}/foocore/cmake")
