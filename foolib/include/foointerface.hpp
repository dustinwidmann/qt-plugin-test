#ifndef FOOINTERFACE_HPP
#define FOOINTERFACE_HPP

#include "foo_global.hpp"

#include <QtCore>
#include <QtPlugin>

class FOO_EXPORT FooInterface
{
    public:
        virtual ~FooInterface();
        virtual void doFoo() = 0;
};
#define FooInterface_iid "foo.FooInterface"
Q_DECLARE_INTERFACE(FooInterface, FooInterface_iid)

#endif
