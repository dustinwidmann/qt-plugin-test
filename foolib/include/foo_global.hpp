#ifndef FOO_GLOBAL_HPP
#define FOO_GLOBAL_HPP

#include <QtCore/qglobal.h>

#if defined(FOO_LIBRARY)
#   define FOO_EXPORT Q_DECL_EXPORT
#else
#   define FOO_EXPORT Q_DECL_IMPORT
#endif

#endif


