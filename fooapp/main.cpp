
#include <QtCore>
#include <QtPlugin>
#include <QtDebug>

#include <foo/foointerface.hpp>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc,argv);
    
    QString pluginPath = "/tmp/foo/lib/plugins/libfooplugin.so";

    QPluginLoader loader(pluginPath);

    if (loader.load())
    {
        qDebug() << "hallelujah!";
    } else
    {
        qDebug() << " failed to load plugin :(";
    }
}
