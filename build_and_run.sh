#!/usr/bin/env bash

rm -rf /tmp/foolib-build /tmp/fooapp-build /tmp/fooplugin-build;
mkdir /tmp/foolib-build /tmp/fooapp-build /tmp/fooplugin-build;
export CWD=$(pwd)
cd /tmp/foolib-build
cmake -GNinja -DCMAKE_INSTALL_PREFIX=/tmp/foo $CWD/foolib
ninja
ninja install
cd /tmp/fooplugin-build
cmake -GNinja -DCMAKE_INSTALL_PREFIX=/tmp/foo $CWD/fooplugin
ninja
ninja install
cd /tmp/fooapp-build
cmake -GNinja -DCMAKE_INSTALL_PREFIX=/tmp/foo $CWD/fooapp
ninja
ninja install


LD_LIBRARY_PATH=/opt/qt-rms/lib:/tmp/foo/lib /tmp/foo/bin/fooapp
